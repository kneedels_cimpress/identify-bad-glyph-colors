﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IdentifyBadGlyphRuns
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Program p = new Program();

            List<string> urlsToCheck = new List<string>
            {
                "https://uds.documents.cimpress.io/v3/documents/70e50ffd-de22-42a1-b7b6-9be4aa034721/revisions/33a74640-d501-49de-a495-caab9e5fbdbd",
                "https://uds.documents.cimpress.io/v3/documents/70e50ffd-de22-42a1-b7b6-9be4aa034721"
            };

            urlsToCheck = new List<string>
            {
                "https://uds.documents.cimpress.io/v3/documents/a9cd5461-8904-43d7-8130-5212b796c4dd"
            };

            Guid guid = Guid.NewGuid();
            string fileName = @"D:\dev\" + guid.ToString() + ".csv";

            urlsToCheck = File.ReadAllLines(@"d:\dev\IdentifyBadGlyphRuns\10-07.txt").ToList();
            //Random r = new Random();
            //urlsToCheck = urlsToCheck.OrderBy(u => r.Next()).Take(100).ToList();

            List<CheckResult> results = new List<CheckResult>();

            int urlNum = 0;
            int recordNum = 0;
            object lockObject = "theLockObject";

            ParallelOptions options = new ParallelOptions {MaxDegreeOfParallelism = 6};

            Parallel.ForEach(urlsToCheck, options, url =>
            {
                Interlocked.Increment(ref urlNum);
                var checkResult = p.CheckUrl(url).Result;
                if (checkResult.ImpactStatus != ImpactStatus.NotImpacted)
                {
                    Interlocked.Increment(ref recordNum);
                    lock (lockObject)
                    {
                        File.AppendAllText(fileName, checkResult.ToString() + "\n");
                        Console.WriteLine(recordNum + "/" + urlNum + ": " + checkResult);
                        results.Add(checkResult);
                    }
                }
            });

            //foreach (var url in urlsToCheck)
            //{
            //    Interlocked.Increment(ref urlNum);
            //    //urlNum++;
            //    var checkResult = await p.CheckUrl(url);
            //    if (checkResult.ImpactStatus != ImpactStatus.NotImpacted)
            //    {
            //        Interlocked.Increment(ref recordNum);
            //        File.AppendAllText(fileName, checkResult.ToString() + "\n");
            //        Console.WriteLine(recordNum + "/" + urlNum + ": " + checkResult);
            //        results.Add(checkResult);
            //    }
            //}

            Console.WriteLine("Done");
        }

        private HttpClient _client;
        public Program()
        {
            _client = new HttpClient();
        }

        public async Task<CheckResult> CheckUrl(string url)
        {
            if (url.Contains("/revisions/"))
            {
                var checkResult = await CheckAdminRevisionUrl(ConvertToAdminUrl(url));
                checkResult.Url = url;
                return checkResult;
            }

            var revisionUrls = await GetRevisionUrls(url);

            string tenant = null;

            foreach (var revisionUrl in revisionUrls)
            {
                var checkResult = await CheckAdminRevisionUrl(ConvertToAdminUrl(revisionUrl));
                tenant = string.IsNullOrEmpty(checkResult.Tenant) ? tenant : checkResult.Tenant;
                if (checkResult.ImpactStatus == ImpactStatus.Impacted)
                {
                    checkResult.Url = url;
                    checkResult.ImpactStatus = revisionUrls.Count == 1 ? ImpactStatus.Impacted : ImpactStatus.LikelyImpacted;
                    return checkResult;
                }
            }

            return new CheckResult(url, ImpactStatus.NotImpacted, tenant);
        }

        public async Task<List<string>> GetRevisionUrls(string documentUrl)
        {

            List<string> revisionUrls = new List<string>();
            string revisionsUrl = documentUrl + "/revisions?pageSize=101";
            using (HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, revisionsUrl))
            {
                requestMessage.Headers.Authorization = AuthenticationHeaderValue.Parse("Bearer <BEARER>");
                using (HttpResponseMessage response = await _client.SendAsync(requestMessage))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception("Could not load revisions " + await response.Content.ReadAsStringAsync());
                    }

                    string json = await response.Content.ReadAsStringAsync();

                    dynamic revisions = JsonConvert.DeserializeObject(json);

                    if (revisions.Count == 0)
                    {
                        throw new Exception("No revisions found");
                    }

                    if (revisions.Count > 100)
                    {
                        throw new Exception("Too many revisions");
                    }

                    foreach (var item in revisions._embedded.item)
                    {
                        string revisionUrl = item._links.self.href;
                        revisionUrls.Add(revisionUrl);
                    }
                }
            }

            return revisionUrls;
        }

        public string ConvertToAdminUrl(string url)
        {
            return url
                .Replace("https://storage.documents.cimpress.io/v3/", "https://storage.documents.cimpress.io/admin/v3/")
                .Replace("https://uds.documents.cimpress.io/v3/", "https://storage.documents.cimpress.io/admin/v3/");
        }

        public async Task<CheckResult> CheckAdminRevisionUrl(string url)
        {
            try
            {
                using (var responseMessage = await _client.GetAsync(url))
                {
                    if (!responseMessage.IsSuccessStatusCode)
                    {
                        throw new Exception("Could not load document " + await responseMessage.Content.ReadAsStringAsync());
                    }

                    var responseString = await responseMessage.Content.ReadAsStringAsync();
                    dynamic response = JsonConvert.DeserializeObject(responseString);

                    string rawGlyphs = response.documentGlyphs;

                    if (rawGlyphs != null && rawGlyphs.StartsWith("{\"GlyphInformation"))
                    {

                        dynamic glyphs = JsonConvert.DeserializeObject(rawGlyphs);

                        foreach (var glyphInformation in glyphs.GlyphInformation)
                        {
                            foreach (var glyphRun in glyphInformation.GlyphRuns)
                            {
                                foreach (var color in glyphRun.Colors)
                                {
                                    if ((color.C == 0 && color.M == 0 && color.Y == 0 && color.K == 0) && 
                                        !(color.R == 255 && color.G == 255 && color.B == 255)) // probably more values map to CMYK white than RGB#FFFFFF
                                    {

                                        string rawDocument = response.document;
                                        dynamic document = JsonConvert.DeserializeObject(rawDocument);
                                        string tenant = document.Tenant;

                                        return new CheckResult(url, ImpactStatus.Impacted, tenant);
                                    }
                                }
                            }
                        }
                    }
                }

                return new CheckResult(url, ImpactStatus.NotImpacted);
            }
            catch (Exception e)
            {
                return new CheckResult(url, ImpactStatus.Unknown);
            }
        }

    }

    

    public class CheckResult
    {
        private static Regex parseIdRegex = new Regex("/documents/(?<docId>[^/]+)/revisions/(?<revisionId>.+)");

        public string Url { get; set; }

        public string DocId { get; set; }

        public string RevisionId { get; set; }

        public ImpactStatus ImpactStatus { get; set; }
        public string Tenant { get; set; }

        public CheckResult(string url, ImpactStatus impactStatus, string tenant = null)
        {
            ImpactStatus = impactStatus;
            Tenant = tenant;
            Url = url;

            var match = parseIdRegex.Match(url);
            DocId = match.Groups["docId"].Value;
            RevisionId = match.Groups["revisionId"].Value;
        }

        public override string ToString()
        {
            return $"{ImpactStatus},{Tenant},{Url},{DocId},{RevisionId}";
        }
    }

    public enum ImpactStatus
    {
        Unknown,
        NotImpacted,
        Impacted,
        LikelyImpacted
    }
}
